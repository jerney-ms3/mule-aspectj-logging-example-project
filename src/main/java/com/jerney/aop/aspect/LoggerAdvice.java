package com.jerney.aop.aspect;

import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.aspectj.lang.JoinPoint;
import org.mule.DefaultMuleEvent;
import org.mule.api.MuleContext;
import org.mule.api.context.MuleContextAware;
import org.mule.util.NetworkUtils;

public class LoggerAdvice implements MuleContextAware {
	final static Logger logger = Logger.getLogger(LoggerAdvice.class);
	private MuleContext ctx = null;
	
	public void beforeLog(JoinPoint jp) {
		DefaultMuleEvent event = (DefaultMuleEvent) jp.getArgs()[0];
		String serverAddress;
		
		try {
			serverAddress = NetworkUtils.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			serverAddress = "Unknown";
		}
		
		ThreadContext.put("flowName",        event.getFlowConstruct().getName());
		ThreadContext.put("applicationName", ctx.getConfiguration().getId());
		ThreadContext.put("serverIP",        serverAddress);
	}

	@Override
	public void setMuleContext(MuleContext context) {
		this.ctx = context;
	}
}
